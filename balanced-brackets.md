# Balanced Brackets

Given a string possibly containing three types of braces ({}, [], ()), write a function that returns a Boolean indicating whether the given string contains a valid nesting of braces.

## Inputs
String = one String

## Outputs
Boolean means either true or false. true if string has balanced brackets; false if it does not.

## Detailed Description
A string is considered balanced if it has as many opening brackets of a given type as it has closing brackets of that same type. No bracket can be left unmatched. All brackets must be correctly nested. 

## Example

braces("(this){(is)an}example")
will return true

braces("(this){(is)an}example}")
will return false because the trailing } is unbalanced

braces("[{'this':'that']}")
will return false because the brackets are not nested correctly
