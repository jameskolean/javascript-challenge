# Character Occurrence

Write a function that takes a string, and returns the unique characters in the string sorted from most frequectly used to least frequently used.


## Input
String = one String

## Output
Array of Arrays = The inner array will be 2 dimensional containing the unique character and the number of times the character was found in the string. The array outer must be sorted from characters with the most occurences to the least occurences.

## Example
occurences("this is a test")
returns [['t',3],[' ',3],['i',2],['h',1],['a',1],['e',1]]
