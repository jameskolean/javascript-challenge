# Perfect numbers

Given an array of integers of length k, find all the integers where |i - reverse(i)| is evenly divisible by k.

## Input 
Array = An array of integers 

## Input 
Array = Array of only those integers that where |i - reverse(i)| is evenly divisible by k


## Example

perfect([120,19])

for the first integer |120 - 21| = 99 which is not divisible evenly by 2 so it is not included 
for the second integer |19 - 91| = 72 which is divisible evenly by 2 so it is included

result
[19]
